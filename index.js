const RoonApi = require('node-roon-api');
const RoonApiTransport = require('node-roon-api-transport');
const RoonApiStatus = require('node-roon-api-status');
const RoonApiSettings = require('node-roon-api-settings');
const noble = require('@abandonware/noble');
const chalk = require('chalk');
const increment = 3;
let core;

const roon = new RoonApi({
    extension_id: 'org.traska.volumeknob',
    display_name: 'VolumeKnob',
    display_version: '1.0.0',
    publisher: 'Jacob Hallenborg',
    email: 'jacob.hallenborg@gmail.com',
    log_level: 'none',

    core_paired: function (core_) {
        core = core_;
        console.log(chalk.green("Paired with Roon Core"));
        core.services.RoonApiTransport.subscribe_zones((response, msg) => {
            if (msg) {
                (msg.zones || []).forEach((z) => {
                    console.log(chalk.gray('Zone', z.display_name));
                    z.outputs.forEach((o) =>
                        console.log(chalk.gray('\tOutputs', o.output_id))
                    );
                });

            }
        });
    },

    core_unpaired: function (core_) {
        console.log(chalk.red("Lost connection with Roon Core"));
    }
});


let my_settings = Object.assign({
    zone: null,
}, roon.load_config("settings") || {});

function make_layout(settings) {
    const layout = {
        values: settings,
        layout: [],
        has_error: false
    };

    layout.layout.push({
        type: "zone",
        title: "Zone",
        setting: "zone",
    });

    return layout;
}

const svc_settings = new RoonApiSettings(roon, {
    get_settings: function (cb) {
        cb(make_layout(my_settings));
    },

    save_settings: function (req, settings) {
        let layout = make_layout(settings.values);
        req.send_complete(layout.has_error ? "NotValid" : "Success", {settings: layout});

        if (!layout.has_error) {
            my_settings = layout.values;
            svc_settings.update_settings(layout);
            roon.save_config("settings", my_settings);
        }
    }
});


const svc_status = new RoonApiStatus(roon);


roon.init_services({
    required_services: [RoonApiTransport],
    provided_services: [svc_settings, svc_status],
});

svc_status.set_status('Extension enabled', false);
roon.start_discovery();


function setVolume(volume) {
    console.log('Volume delta', volume)
    core.services.RoonApiTransport.change_volume(my_settings.zone, 'relative', volume);
}

function turnUp() {
    setVolume(increment);
}

function turnDown() {
    setVolume(-increment);
}


noble.on('stateChange', async (state) => {
    if (state === 'poweredOn') {
        console.log(chalk.magenta('BLE powered on'));
        await noble.startScanningAsync(["25598cf7424040a69910080f19f91ebc"], false);
    }
});


noble.on('discover', async (peripheral) => {
    console.log(chalk.magenta('Found PowerMate: ', peripheral));
    await noble.stopScanningAsync();

    peripheral.connect(function (error) {

            if (error)
                console.error(error)

            if (peripheral.state === 'connected') {
                console.log('state:' + peripheral.state)
                console.log(chalk.magenta('Connected'));

                svc_status.set_status('PowerMate remote connected', false);
                readRemote(peripheral)
            }
        }
    )
});

async function readRemote(peripheral) {
    const {characteristics} = await peripheral.discoverSomeServicesAndCharacteristicsAsync(null, ['9cf53570ddd947f3ba6309acefc60415']);
    console.log(characteristics);

    characteristics[0].subscribeAsync()
        .then(error => {
            if (error) {
                console.error(error)
            } else {
                console.log('subscribing')
            }
        })
        .catch(err => console.error(err));

    characteristics[0].on('read', (data) => {
        let value = data.readUIntLE(0, data.length);
        if (value === 103)
            turnDown();
        else if (value === 104)
            turnUp();
    });

}
